package fr.jv.pokertests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.jv.pokerhands.Carte;

class CarteTest {

	Carte roiTrefle = new Carte("Trèfle", "roi");
	Carte deuxTrefle = new Carte("Trèfle", "deux");
	Carte deuxCoeur = new Carte("Coeur", "deux");
	
	
	@Test
	void testCompareTo() {
		assertEquals(-1, deuxTrefle.compareTo(roiTrefle));
		assertEquals(0, deuxTrefle.compareTo(deuxTrefle));
		assertEquals(0, deuxTrefle.compareTo(deuxCoeur));
		assertEquals(1, roiTrefle.compareTo(deuxTrefle));
		assertEquals(1, roiTrefle.compareTo(deuxCoeur));
		
	}

	@Test
	void testEstPlusPetiteQue() {
		assertTrue(deuxTrefle.estPlusPetiteQue(roiTrefle));
		assertFalse(deuxTrefle.estPlusPetiteQue(deuxCoeur));
		assertFalse(roiTrefle.estPlusPetiteQue(deuxTrefle));
	}

	@Test
	void testEstPlusGrandeQue() {
		assertTrue(roiTrefle.estPlusGrandeQue(deuxCoeur));
		assertFalse(roiTrefle.estPlusGrandeQue(roiTrefle));
		assertTrue(roiTrefle.estPlusGrandeQue(deuxTrefle));
	}

	@Test
	void testALaMemeValeurQue() {
		assertTrue(deuxTrefle.aLaMemeValeurQue(deuxCoeur));
		assertFalse(roiTrefle.aLaMemeValeurQue(deuxTrefle));
	}
	
	@Test
	void testALaMemeCouleurQue() {
		assertTrue(deuxTrefle.aLaMemeCouleurQue(roiTrefle));
		assertFalse(deuxTrefle.aLaMemeCouleurQue(deuxCoeur));
		
	}

}
