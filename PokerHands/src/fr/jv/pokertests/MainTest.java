package fr.jv.pokertests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import fr.jv.pokerhands.Carte;
import fr.jv.pokerhands.MainJoueur;

class MainTest {
	MainJoueur highQuinteFlushCoeur = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("Roi", "Coeur"),
			new Carte("Dame", "Coeur"), new Carte("Valet", "Coeur"), new Carte("Dix", "Coeur")));
	MainJoueur highQuinteFlushPique = new MainJoueur(Arrays.asList(new Carte("As", "Pique"), new Carte("Roi", "Pique"),
			new Carte("Dame", "Pique"), new Carte("Valet", "Pique"), new Carte("Dix", "Pique")));
	MainJoueur lowQuinteFlush = new MainJoueur(Arrays.asList(new Carte("2", "Pique"), new Carte("3", "Pique"),
			new Carte("4", "Pique"), new Carte("5", "Pique"), new Carte("6", "Pique")));

	MainJoueur highCarre = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("As", "Trèfle"),
			new Carte("As", "Carreau"), new Carte("As", "Pique"), new Carte("Roi", "Pique")));
	MainJoueur highCarre2 = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("As", "Trèfle"),
			new Carte("As", "Carreau"), new Carte("As", "Pique"), new Carte("2", "Pique")));
	MainJoueur lowCarre = new MainJoueur(Arrays.asList(new Carte("3", "Coeur"), new Carte("4", "Trèfle"),
			new Carte("4", "Carreau"), new Carte("4", "Coeur"), new Carte("4", "Pique")));

	MainJoueur highFull = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Roi", "Carreau"), new Carte("As", "Pique"), new Carte("As", "Carreau")));
	MainJoueur mediumFull = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Roi", "Carreau"), new Carte("2", "Pique"), new Carte("2", "Carreau")));
	MainJoueur lowFull = new MainJoueur(Arrays.asList(new Carte("5", "Coeur"), new Carte("5", "Trèfle"),
			new Carte("4", "Carreau"), new Carte("4", "Coeur"), new Carte("4", "Pique")));

	MainJoueur highFlushPique = new MainJoueur(Arrays.asList(new Carte("As", "Pique"), new Carte("Roi", "Pique"),
			new Carte("Dame", "Pique"), new Carte("2", "Pique"), new Carte("8", "Pique")));
	MainJoueur highFlushTrefle = new MainJoueur(Arrays.asList(new Carte("As", "Trèfle"), new Carte("Roi", "Trèfle"),
			new Carte("Dame", "Trèfle"), new Carte("2", "Trèfle"), new Carte("8", "Trèfle")));
	MainJoueur mediumFlush = new MainJoueur(Arrays.asList(new Carte("As", "Pique"), new Carte("Roi", "Pique"),
			new Carte("Dame", "Pique"), new Carte("2", "Pique"), new Carte("7", "Pique")));
	MainJoueur lowFlush = new MainJoueur(Arrays.asList(new Carte("8", "Coeur"), new Carte("3", "Coeur"),
			new Carte("4", "Coeur"), new Carte("5", "Coeur"), new Carte("6", "Coeur")));

	MainJoueur highQuinte = new MainJoueur(Arrays.asList(new Carte("As", "Pique"), new Carte("Roi", "Coeur"),
			new Carte("Dame", "Trèfle"), new Carte("Valet", "Pique"), new Carte("Dix", "Pique")));
	MainJoueur lowQuinte = new MainJoueur(Arrays.asList(new Carte("2", "Pique"), new Carte("3", "Coeur"),
			new Carte("4", "Trèfle"), new Carte("5", "Pique"), new Carte("6", "Pique")));

	MainJoueur highBrelan = new MainJoueur(Arrays.asList(new Carte("Valet", "Coeur"), new Carte("Valet", "Trèfle"),
			new Carte("Valet", "Carreau"), new Carte("2", "Pique"), new Carte("As", "Coeur")));
	MainJoueur highBrelan2 = new MainJoueur(Arrays.asList(new Carte("Valet", "Coeur"), new Carte("Valet", "Trèfle"),
			new Carte("Valet", "Carreau"), new Carte("2", "Pique"), new Carte("3", "Coeur")));
	MainJoueur lowBrelan = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("4", "Carreau"), new Carte("4", "Coeur"), new Carte("4", "Pique")));

	MainJoueur veryHighTwoPair = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Dame", "Carreau"), new Carte("Dame", "Pique"), new Carte("Valet", "Coeur")));
	MainJoueur highTwoPair = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Dame", "Carreau"), new Carte("Dame", "Pique"), new Carte("Dix", "Coeur")));
	MainJoueur mediumTwoPair = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Valet", "Carreau"), new Carte("Valet", "Pique"), new Carte("5", "Coeur")));
	MainJoueur lowTwoPair = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Dix", "Carreau"), new Carte("Dix", "Pique"), new Carte("As", "Coeur")));

	MainJoueur veryHighPair = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("As", "Trèfle"),
			new Carte("Roi", "Carreau"), new Carte("3", "Pique"), new Carte("4", "Coeur")));
	MainJoueur highPair = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("As", "Trèfle"),
			new Carte("2", "Carreau"), new Carte("3", "Pique"), new Carte("4", "Coeur")));
	MainJoueur lowPair = new MainJoueur(Arrays.asList(new Carte("6", "Coeur"), new Carte("6", "Trèfle"),
			new Carte("As", "Carreau"), new Carte("Roi", "Pique"), new Carte("Dame", "Coeur")));

	MainJoueur veryHigh = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Dame", "Carreau"), new Carte("Valet", "Pique"), new Carte("9", "Coeur")));
	MainJoueur high = new MainJoueur(Arrays.asList(new Carte("As", "Coeur"), new Carte("Roi", "Trèfle"),
			new Carte("Valet", "Carreau"), new Carte("Dix", "Pique"), new Carte("9", "Coeur")));
	MainJoueur mediumAs = new MainJoueur(Arrays.asList(new Carte("As", "Carreau"), new Carte("5", "Pique"),
			new Carte("4", "Carreau"), new Carte("3", "Pique"), new Carte("2", "Coeur")));
	MainJoueur mediumRoi = new MainJoueur(Arrays.asList(new Carte("Roi", "Coeur"), new Carte("Dame", "Trèfle"),
			new Carte("Valet", "Coeur"), new Carte("Dix", "Carreau"), new Carte("8", "Coeur")));
	MainJoueur low = new MainJoueur(Arrays.asList(new Carte("7", "Coeur"), new Carte("5", "Trèfle"),
			new Carte("4", "Carreau"), new Carte("3", "Pique"), new Carte("2", "Coeur")));

	MainJoueur plusPetiteQuinte = new MainJoueur(Arrays.asList(new Carte("as", "coeur"), new Carte("2", "trèfle"), new Carte("3", "coeur"), new Carte("4", "pique"),  new Carte("5", "trèfle")));

//	@Test
	void tesNoCombination() {
		assertTrue(veryHigh.compareTo(veryHigh) == 0);
		assertTrue(veryHigh.compareTo(high) > 0);
		assertTrue(veryHigh.compareTo(low) > 0);
		assertTrue(high.compareTo(low) > 0);
		assertTrue(high.compareTo(veryHigh) < 0);
		assertTrue(veryHigh.compareTo(lowQuinteFlush) < 0);
		assertTrue(veryHigh.compareTo(lowCarre) < 0);
		assertTrue(veryHigh.compareTo(lowFull) < 0);
		assertTrue(veryHigh.compareTo(lowFlush) < 0);
		assertTrue(veryHigh.compareTo(lowQuinte) < 0);
		assertTrue(veryHigh.compareTo(lowFull) < 0);
		assertTrue(veryHigh.compareTo(lowTwoPair) < 0);
		assertTrue(veryHigh.compareTo(lowPair) < 0);
		assertTrue(mediumRoi.compareTo(mediumAs) < 0);
		assertTrue(mediumAs.compareTo(mediumRoi) > 0);
	}

	@Test
	void testPair() {
//		assertTrue(veryHighPair.compareTo(veryHighPair) == 0);
//		assertTrue(veryHighPair.compareTo(highPair) > 0);
//		assertTrue(veryHighPair.compareTo(lowPair) > 0);
//		assertTrue(highPair.compareTo(lowPair) > 0);
//		assertTrue(lowPair.compareTo(veryHigh) > 0);
//		assertTrue(highPair.compareTo(veryHighPair) < 0);
//		assertTrue(veryHighPair.compareTo(lowQuinteFlush) < 0);
//		assertTrue(veryHighPair.compareTo(lowCarre) < 0);
//		assertTrue(veryHighPair.compareTo(lowFull) < 0);
//		assertTrue(veryHighPair.compareTo(lowFlush) < 0);
//		assertTrue(veryHighPair.compareTo(lowQuinte) < 0);
//		assertTrue(veryHighPair.compareTo(lowFull) < 0);
		assertTrue(veryHighPair.compareTo(lowTwoPair) < 0);
	}

//	@Test
	void testTwoPair() {
		assertTrue(highTwoPair.compareTo(highTwoPair) == 0);
		assertTrue(veryHighTwoPair.compareTo(highTwoPair) > 0);
		assertTrue(highTwoPair.compareTo(mediumTwoPair) > 0);
		assertTrue(highTwoPair.compareTo(lowTwoPair) > 0);
		assertTrue(mediumTwoPair.compareTo(lowTwoPair) > 0);
		assertTrue(lowTwoPair.compareTo(veryHighPair) > 0);
		assertTrue(lowTwoPair.compareTo(veryHigh) > 0);
		assertTrue(lowTwoPair.compareTo(highTwoPair) < 0);
		assertTrue(highTwoPair.compareTo(lowQuinteFlush) < 0);
		assertTrue(highTwoPair.compareTo(lowCarre) < 0);
		assertTrue(highTwoPair.compareTo(lowFull) < 0);
		assertTrue(highTwoPair.compareTo(lowFlush) < 0);
		assertTrue(highTwoPair.compareTo(lowQuinte) < 0);
		assertTrue(highTwoPair.compareTo(lowFull) < 0);
	}

//	@Test
	void testBrelan() {
		assertTrue(highBrelan.compareTo(highBrelan) == 0);
		assertTrue(highBrelan.compareTo(highBrelan2) > 0);
		assertTrue(highBrelan.compareTo(lowBrelan) > 0);
		assertTrue(lowBrelan.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowBrelan.compareTo(veryHighPair) > 0);
		assertTrue(lowBrelan.compareTo(veryHigh) > 0);
		assertTrue(lowBrelan.compareTo(highBrelan) < 0);
		assertTrue(highBrelan.compareTo(lowQuinteFlush) < 0);
		assertTrue(highBrelan.compareTo(lowCarre) < 0);
		assertTrue(highBrelan.compareTo(lowFull) < 0);
		assertTrue(highBrelan.compareTo(lowFlush) < 0);
		assertTrue(highBrelan.compareTo(lowQuinte) < 0);
	}

//	@Test
	void testQuinte() {
		assertTrue(highQuinte.compareTo(highQuinte) == 0);
		assertTrue(highQuinte.compareTo(lowQuinte) > 0);
		assertTrue(lowQuinte.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowQuinte.compareTo(veryHighPair) > 0);
		assertTrue(lowQuinte.compareTo(veryHigh) > 0);
		assertTrue(lowQuinte.compareTo(highQuinte) < 0);
		assertTrue(highQuinte.compareTo(lowQuinteFlush) < 0);
		assertTrue(highQuinte.compareTo(lowCarre) < 0);
		assertTrue(highQuinte.compareTo(lowFull) < 0);
		assertTrue(highQuinte.compareTo(lowFlush) < 0);
		assertTrue(plusPetiteQuinte.compareTo(veryHighTwoPair) > 0);
		assertTrue(plusPetiteQuinte.compareTo(veryHighPair) > 0);
		
	}

//	@Test
	void testFlush() {
		assertTrue(highFlushPique.compareTo(highFlushTrefle) == 0);
		assertTrue(highFlushPique.compareTo(mediumFlush) > 0);
		assertTrue(highFlushPique.compareTo(lowFlush) > 0);
		assertTrue(mediumFlush.compareTo(lowFlush) > 0);
		assertTrue(lowFlush.compareTo(highQuinte) > 0);
		assertTrue(lowFlush.compareTo(highBrelan) > 0);
		assertTrue(lowFlush.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowFlush.compareTo(veryHighPair) > 0);
		assertTrue(lowFlush.compareTo(veryHigh) > 0);
		assertTrue(lowFlush.compareTo(highFlushPique) < 0);
		assertTrue(highFlushPique.compareTo(lowQuinteFlush) < 0);
		assertTrue(highFlushPique.compareTo(lowCarre) < 0);
		assertTrue(highFlushPique.compareTo(lowFull) < 0);
	}
//
//	@Test
	void testFull() {
		assertTrue(highFull.compareTo(mediumFull) > 0);
		assertTrue(highFull.compareTo(lowFull) > 0);
		assertTrue(lowFull.compareTo(highQuinte) > 0);
		assertTrue(lowFull.compareTo(highFlushPique) > 0);
		assertTrue(lowFull.compareTo(highQuinte) > 0);
		assertTrue(lowFull.compareTo(highBrelan) > 0);
		assertTrue(lowFull.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowFull.compareTo(veryHighPair) > 0);
		assertTrue(lowFull.compareTo(veryHigh) > 0);
		assertTrue(lowFull.compareTo(highFull) < 0);
		assertTrue(highFull.compareTo(lowQuinteFlush) < 0);
		assertTrue(highFull.compareTo(lowCarre) < 0);
	}

//	@Test
	void testCarre() {
		assertTrue(highCarre.compareTo(lowCarre) > 0);
		assertTrue(highCarre.compareTo(highCarre) == 0);
		assertTrue(highCarre.compareTo(highCarre2) > 0);
		assertTrue(highCarre.compareTo(highQuinte) > 0);
		assertTrue(highCarre.compareTo(highFull) > 0);
		assertTrue(highCarre.compareTo(highFlushPique) > 0);
		assertTrue(highCarre.compareTo(highQuinte) > 0);
		assertTrue(highCarre.compareTo(highBrelan) > 0);
		assertTrue(highCarre.compareTo(veryHighTwoPair) > 0);
		assertTrue(highCarre.compareTo(veryHighPair) > 0);
		assertTrue(highCarre.compareTo(veryHigh) > 0);
		assertTrue(lowCarre.compareTo(highCarre) < 0);
		assertTrue(highCarre.compareTo(lowQuinteFlush) < 0);
		assertTrue(lowCarre.compareTo(highCarre) < 0);
	}

//	@Test
	void testQuinteFlush() {
		assertTrue(highQuinteFlushCoeur.compareTo(highQuinteFlushPique) == 0);
		assertTrue(highQuinteFlushCoeur.compareTo(lowQuinteFlush) > 0);
		assertTrue(lowQuinteFlush.compareTo(highQuinte) > 0);
		assertTrue(lowQuinteFlush.compareTo(highCarre) > 0);
		assertTrue(lowQuinteFlush.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowQuinteFlush.compareTo(highFull) > 0);
		assertTrue(lowQuinteFlush.compareTo(highFlushPique) > 0);
		assertTrue(lowQuinteFlush.compareTo(highBrelan) > 0);
		assertTrue(lowQuinteFlush.compareTo(veryHighTwoPair) > 0);
		assertTrue(lowQuinteFlush.compareTo(veryHighPair) > 0);
		assertTrue(lowQuinteFlush.compareTo(veryHigh) > 0);
		assertTrue(lowQuinteFlush.compareTo(highQuinteFlushPique) < 0);
	}
}
