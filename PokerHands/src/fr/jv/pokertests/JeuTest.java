package fr.jv.pokertests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.jv.pokerhands.Carte;
import fr.jv.pokerhands.Jeu;

class JeuTest {

	Jeu monJeu = new Jeu();
	Jeu monJeuDe32Cartes = new Jeu(5);
	Carte deuxDeCoeur = new Carte("2", "coeur");
	Carte troisDeCoeur = new Carte("3", "coeur");
	Carte quatreDeCoeur = new Carte("4", "coeur");
	Carte cinqDeCoeur = new Carte("5", "coeur");
	Carte sixDeCoeur = new Carte("6", "coeur");
	Carte septDeCoeur = new Carte("7", "coeur");
	

	@Test
	void initJeu() {
		assertEquals(52, monJeu.getNombreDeCartes());
		assertEquals(32, monJeuDe32Cartes.getNombreDeCartes());
		assertTrue(monJeu.getJeuDeCartes().contains(deuxDeCoeur));
		assertTrue(monJeu.getJeuDeCartes().contains(deuxDeCoeur));
		assertTrue(monJeu.getJeuDeCartes().contains(troisDeCoeur));
		assertTrue(monJeu.getJeuDeCartes().contains(quatreDeCoeur));
		assertTrue(monJeu.getJeuDeCartes().contains(cinqDeCoeur));
		assertTrue(monJeu.getJeuDeCartes().contains(sixDeCoeur));
		
		
		
		
		
		assertFalse(monJeuDe32Cartes.getJeuDeCartes().contains(deuxDeCoeur));
		assertFalse(monJeuDe32Cartes.getJeuDeCartes().contains(troisDeCoeur));
		assertFalse(monJeuDe32Cartes.getJeuDeCartes().contains(quatreDeCoeur));
		assertFalse(monJeuDe32Cartes.getJeuDeCartes().contains(cinqDeCoeur));
		assertFalse(monJeuDe32Cartes.getJeuDeCartes().contains(sixDeCoeur));
		assertTrue(monJeuDe32Cartes.getJeuDeCartes().contains(septDeCoeur));
		
	}

}
//
