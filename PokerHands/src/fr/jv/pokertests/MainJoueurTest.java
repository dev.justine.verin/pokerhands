package fr.jv.pokertests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import fr.jv.pokerhands.Carte;
import fr.jv.pokerhands.MainJoueur;

class MainJoueurTest {

	MainJoueur mainJoueur1 = new MainJoueur();
	Carte carte1 = new Carte("roi", "pique");
	Carte carte2 = new Carte("reine", "coeur");
	Carte[] quinteFlush = {new Carte("deux", "coeur"), new Carte("trois", "coeur"), new Carte("quatre", "coeur"), new Carte("cinq", "coeur"), new Carte("six", "coeur")};
	Carte[] doublePaire = {new Carte("deux", "coeur"), new Carte("pique", "deux"), new Carte("trefle", "valet"), new Carte("carreau", "as"), new Carte("carreau", "valet")};
	Carte [] brelan = {new Carte("deux", "coeur"), new Carte("deux", "pique"), new Carte("carreau", "deux"), new Carte("carreau", "as"), new Carte("carreau", "valet")};
	

	@Test
	void testRecevoirCarte() throws Exception {
		mainJoueur1.recevoirCarte(carte1);
		int nombreDeCartesRecues = mainJoueur1.getNombreDeCartes();
		assertEquals(1, nombreDeCartesRecues);
		assertEquals(carte1, mainJoueur1.getCartes().get(nombreDeCartesRecues - 1));

	}

	@Test
	void testRecevoirCartes() throws Exception {
		Carte[] cartesARecevoir = { carte1, carte2 };
		mainJoueur1.recevoirNCartes(cartesARecevoir);
		int nombreDeCartesRecues = mainJoueur1.getNombreDeCartes();
		assertEquals(cartesARecevoir.length, nombreDeCartesRecues);
		for (int indice = 0; indice < cartesARecevoir.length - 1; indice++) {
			assertEquals(cartesARecevoir[indice], mainJoueur1.getCartes().get(nombreDeCartesRecues - 1 - indice));

		}

	}
	
	
	@Test
	void testRendreCarte() throws Exception{
		Carte[] cartesARecevoir = { carte1, carte2 };
		mainJoueur1.recevoirNCartes(cartesARecevoir);
		Carte carteRendue = mainJoueur1.rendreCarte();
		assertEquals(cartesARecevoir[0], carteRendue);
		assertEquals(-1, mainJoueur1.getCartes().indexOf(carteRendue));
		assertEquals(1, mainJoueur1.getNombreDeCartes());
		
		
	}
	
	@Test
	void testRendreNCartes() throws Exception{
		Carte[] cartesARecevoir = { carte1, carte2 };
		mainJoueur1.recevoirNCartes(cartesARecevoir);
		Carte[] cartesRendues = mainJoueur1.rendreCartes(2);
		assertEquals(0, mainJoueur1.getNombreDeCartes());
		for(int indice = 0; indice < cartesARecevoir.length; indice++) {
			assertEquals(-1, mainJoueur1.getCartes().indexOf(cartesRendues[indice]));
			assertEquals(cartesARecevoir[indice], cartesRendues[indice]);
			
		}
	}
	

	
}
