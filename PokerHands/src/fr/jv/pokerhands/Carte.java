package fr.jv.pokerhands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

/**
 * Classe Carte modélisant une carte d'un jeu de cartes à partir des constantes de la classe Jeu
 * Comparable à d'autres cartes en fonction de la valeur de l'attribut hauteur de l'instance
 */
public class Carte implements Comparable<Carte> {
	private String couleur;
	private String hauteur;
	private static final String[] LISTE_HAUTEURS = Jeu.LISTE_HAUTEURS;

	private ArrayList<String> listeHauteurs;


	/**
	 * Constructeur de l'instance de Carte Crée l'instance et instancie l'attribut
	 * listeHauteurs
	 * 
	 * @param hauteur : String définissant la hauteur de la carte
	 * @param couleur : String définissant la couleur de la carte
	 */
	public Carte(String hauteur, String couleur) {
		super();
		this.couleur = couleur;
		this.hauteur = hauteur;
		listeHauteurs = new ArrayList<>();
		Collections.addAll(listeHauteurs, LISTE_HAUTEURS);
	}

	


	/**
	 * Renvoie le résultat de la comparaison entre l'instance de Carte et une autre
	 * instance de Carte en paramètre
	 * 
	 * @param autreCarte une instance de Carte
	 * @return true si la carte est plus faible que autreCarte, false sinon
	 */
	public boolean estPlusPetiteQue(Carte autreCarte) {
		return compareTo(autreCarte) < 0;

	}

	/**
	 * Renvoie le résultat de la comparaison entre l'instance de Carte et une autre
	 * instance de Carte en paramètre
	 * 
	 * @param autreCarte une instance de Carte
	 * @return true si la carte est plus forte que autreCarte, false sinon
	 */

	public boolean estPlusGrandeQue(Carte autreCarte) {
		return compareTo(autreCarte) > 0;

	}

	/**
	 * Renvoie le résultat de la comparaison entre l'instance de Carte et une autre
	 * instance de Carte en paramètre
	 * 
	 * @param autreCarte une instance de Carte
	 * @return true si la carte a la même valeur que autreCarte, false sinon
	 */

	public boolean aLaMemeValeurQue(Carte autreCarte) {
		return compareTo(autreCarte) == 0;

	}

	/**
	 * Renvoie le résultat du test equalsIgnoreCase entre la couleur de l'instance
	 * de Carte et la couleur d'une autre instance de Carte
	 * 
	 * @param autreCarte une instance de Carte
	 * @return true si les deux cartes sont de la même couleur, false sinon
	 */
	public boolean aLaMemeCouleurQue(Carte autreCarte) {
		return this.couleur.equalsIgnoreCase(autreCarte.couleur);

	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getHauteur() {
		return hauteur;
	}

	public void setHauteur(String hauteur) {
		this.hauteur = hauteur;
	}

	@Override
	public String toString() {
//		return "Carte = [Couleur = " + couleur + ", " + "hauteur = " + hauteur + "]";
		return hauteur + " de " + couleur;
	}




	@Override
	public int hashCode() {
		return Objects.hash(couleur, hauteur);
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carte other = (Carte) obj;
		return Objects.equals(couleur, other.couleur) && Objects.equals(hauteur, other.hauteur);
	}




	@Override
	public int compareTo(Carte autreCarte) {
		Integer indexHauteur = listeHauteurs.indexOf(this.hauteur.toLowerCase());
		Integer indexHauteurAutreCarte = listeHauteurs.indexOf(autreCarte.hauteur.toLowerCase());
		return indexHauteur.compareTo(indexHauteurAutreCarte);
	}

}
