package fr.jv.pokerhands;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class ConstructeurCotation {

	private static final char[] COTATIONS = { 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N' };
	private HashMap<Integer, Integer> carteHauteursValeurs;
	private HashMap<String, Integer> carteCouleursValeurs;
	private List<Carte> cartes;
	private int[] tableauCles;
	private boolean estSuite;
	private boolean estCouleur;
	private boolean estCarre;
	private boolean estBrelan;
	private int nbPaires;
	private int hauteurPlusHauteCarte;
	private int hauteurPlusBasseCarte;
	private int hauteurCarre;
	private int hauteurBrelan;
	private int hauteurPaireHaute;
	private int hauteurPaireBasse;
	

	public ConstructeurCotation(MainJoueur mainAAnalyser) {
		carteHauteursValeurs = new HashMap<>();
		carteCouleursValeurs = new HashMap<>();
		cartes = mainAAnalyser.getCartes();
		genererTableauCles();
		estSuite = false;
		estCouleur = false;
		estBrelan = false;
		nbPaires = 0;
		hauteurPlusHauteCarte = -1;
		hauteurPlusBasseCarte = -1;
		

	}

	public String genererCotation() {
		lireHauteursEtCouleurs(carteHauteursValeurs, carteCouleursValeurs, cartes);
		genererTableauCles();
		determinerCombinaisons();
		char[] caracteresCombinaison = instancierCharsCombinaisons();
		assignerValeursCombinaison(caracteresCombinaison);
		StringBuilder cotationCombinaison = new StringBuilder("");
		for(char c : caracteresCombinaison) {
			cotationCombinaison.append(c);
		}
		String cotationCartesIndividuelles = construireCotationCartesIndividuelles();
		return cotationCombinaison.toString() + cotationCartesIndividuelles;
	}
	
	private String construireCotationCartesIndividuelles() {

		StringBuilder cotationsCartes = new StringBuilder("");
		for (Entry<Integer, Integer> cleValeur : carteHauteursValeurs.entrySet()) {
			concatenerCotationsIndividuelles(cotationsCartes, cleValeur);
		}
		return cotationsCartes.toString();

	}

	private void concatenerCotationsIndividuelles(StringBuilder cotationsCartes, Entry<Integer, Integer> cleValeur) {
		int nombreLettres = cleValeur.getValue();
		while (nombreLettres > 0) {
			cotationsCartes.insert(0, COTATIONS[cleValeur.getKey()]);
			nombreLettres--;

		}
		
	}

	private void assignerValeursCombinaison(char[] caracteresCombinaison) {
		if (estSuite && estCouleur && hauteurPlusHauteCarte == COTATIONS.length - 1) {
			caracteresCombinaison[0] = COTATIONS[hauteurPlusHauteCarte];
			return;
		}
		if (estSuite && estCouleur) {
			caracteresCombinaison[1] = COTATIONS[hauteurPlusHauteCarte];
			return;
		}
		if (estCarre) {
			caracteresCombinaison[2] = COTATIONS[hauteurCarre];
			return;
		}
		if (estBrelan && nbPaires == 1) {
			caracteresCombinaison[3] = COTATIONS[hauteurBrelan];
			return;
		}

		if (estCouleur) {
			caracteresCombinaison[4] = COTATIONS[hauteurPlusHauteCarte];
			return;
		}
		if (estSuite) {
			caracteresCombinaison[5] = COTATIONS[hauteurPlusHauteCarte];
			return;
		}
		if (estBrelan) {
			caracteresCombinaison[6] = COTATIONS[hauteurBrelan];
			return;
		}
		if (nbPaires > 1) {
			caracteresCombinaison[7] = COTATIONS[hauteurPaireHaute];
			caracteresCombinaison[8] = COTATIONS[hauteurPaireBasse];
			return;
		}
		if(nbPaires > 0) {
			caracteresCombinaison[8] = COTATIONS[hauteurPaireBasse];
			
		}

	}

	private char[] instancierCharsCombinaisons() {
		char[] caracteresCombinaison = new char[9];
		for (int indice = 0; indice < caracteresCombinaison.length; indice++) {
			caracteresCombinaison[indice] = 'A';

		}
		return caracteresCombinaison;
	}

	private void determinerCombinaisons() {
		estSuite = estSuite();
		estCouleur = carteCouleursValeurs.size() == 1;
		determinerPairesEtPlus();
		determinerHauteurCarteHaute();
		determinerHauteurCarteBasse();

	}

	private void determinerHauteurCarteBasse() {
		hauteurPlusBasseCarte = tableauCles[0];

	}

	private void determinerHauteurCarteHaute() {
		if (estSuite && hauteurPlusBasseCarte == 0) {
			hauteurPlusHauteCarte = tableauCles[tableauCles.length - 1];
			return;

		}
		hauteurPlusHauteCarte = tableauCles[tableauCles.length - 1];

	}

	private void determinerPairesEtPlus() {
		if (estSuite || estCouleur)
			return;
		for (Entry<Integer, Integer> cleValeur : carteHauteursValeurs.entrySet()) {
			determinerValeurPaireEtPlus(cleValeur);

		}

	}

	private void determinerValeurPaireEtPlus(Entry<Integer, Integer> cleValeur) {
		switch (cleValeur.getValue()) {
		case 2 -> assignerValeurPaires(cleValeur);

		case 3 -> {
			hauteurBrelan = cleValeur.getKey();
			estBrelan = true;
		}
		case 4 -> {
			hauteurCarre = cleValeur.getKey();
			estCarre = true;

		}
		}

	}

	private void assignerValeurPaires(Entry<Integer, Integer> cleValeur) {
		if (nbPaires >= 1) {
			hauteurPaireHaute = cleValeur.getKey();
			nbPaires++;
			return;

		}
		hauteurPaireBasse = cleValeur.getKey();
		nbPaires++;
	}

	private void lireHauteursEtCouleurs(HashMap<Integer, Integer> carteHauteursValeurs,
			HashMap<String, Integer> carteCouleursValeurs, List<Carte> cartes) {
		String[] listeHauteurs = Jeu.LISTE_HAUTEURS;
		for (Carte carte : cartes) {
			ajouterCleValeurOuIncrementValeur(carteHauteursValeurs, carteCouleursValeurs, listeHauteurs, carte);

		}

	}

	private void genererTableauCles() {
		int indice = 0;
		tableauCles = new int[carteHauteursValeurs.size()];
		for (int cle : carteHauteursValeurs.keySet()) {
			tableauCles[indice] = cle;
			indice++;
		}

	}

	private boolean estSuite() {
		estSuite = suiteDeCartesDepuisOrigine(0, 3, tableauCles);
		boolean estPetiteSuite = estPetiteQuinte(tableauCles.length - 2, tableauCles);
		return (estSuite && suiteDeCartesDepuisOrigine(3, 1, tableauCles) || estPetiteSuite);

	}

	private boolean estPetiteQuinte(int indice, int[] tableauCles) {
		return estHauteurDonnee("5", tableauCles, indice) && (estHauteurDonnee("as", tableauCles, indice + 1));
	}

	private boolean estHauteurDonnee(String hauteurRecherchee, int[] tableauCles, int position) {
		String[] listeHauteurs = Jeu.LISTE_HAUTEURS;
		int indice = 0;
		while (!listeHauteurs[indice].equalsIgnoreCase(hauteurRecherchee) && indice < listeHauteurs.length) {
			indice++;

		}
		return indice == tableauCles[position];
	}

	private boolean suiteDeCartesDepuisOrigine(int origine, int nombreDeCartes, int[] tableauCles) {
		boolean estQuinte = true;
		int indice = origine;
		while (estQuinte && indice < origine + nombreDeCartes - 1) {
			estQuinte = tableauCles[indice] == tableauCles[indice + 1] - 1;
			indice++;

		}
		return estQuinte;
	}

	private void ajouterCleValeurOuIncrementValeur(HashMap<Integer, Integer> carteHauteursValeurs,
			HashMap<String, Integer> carteCouleursValeurs, String[] listeHauteurs, Carte carte) {
		String hauteur = carte.getHauteur();
		int indexOfHauteur = findIndexHauteur(listeHauteurs, hauteur);
		String couleur = carte.getCouleur();
		carteHauteursValeurs.put(indexOfHauteur, carteHauteursValeurs.getOrDefault(indexOfHauteur, 0) + 1);
		carteCouleursValeurs.put(couleur, carteCouleursValeurs.getOrDefault(couleur, 0) + 1);

	}

	private int findIndexHauteur(String[] listeHauteurs, String hauteur) {
		int indexHauteur = -1;
		int indice = 0;
		while (indexHauteur == -1 && indice < listeHauteurs.length) {
			indexHauteur = retournerIndexIfStringEquals(listeHauteurs, hauteur, indexHauteur, indice);
			indice++;

		}
		return indexHauteur;
	}

	private int retournerIndexIfStringEquals(String[] listeHauteurs, String hauteur, int indexHauteur, int indice) {
		if (listeHauteurs[indice].equalsIgnoreCase(hauteur)) {
			indexHauteur = indice;
		}
		return indexHauteur;
	}

}
