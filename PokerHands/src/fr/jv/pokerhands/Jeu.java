package fr.jv.pokerhands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Classe modélisant un jeu traditionnel de 52 cartes Génère 52 instances de
 * classe Carte (4 couleurs * 13 hauteurs), distribue et reçoit des cartes,
 * mélange les cartes
 */



//TO DO : paramétrer pour créer un jeu de 32 cartes ---> LISTE HAUTEURS aller de 7 à as;

public class Jeu {

	protected static final String[] LISTE_HAUTEURS = { "2", "3", "4", "5", "6", "7", "8", "9", "dix", "valet", "dame",
			"roi", "as" };

	public static String[] getListeHauteurs() {
		return LISTE_HAUTEURS;
	}

	protected static final String[] LISTE_COULEURS = { "trèfle", "pique", "carreau", "coeur" };
	private ArrayList<Carte> jeuDeCartes;
	private Queue<Carte> talon;
	private int nombreDeCartes = 0;
	private int nombreMaximumDeCartes = LISTE_HAUTEURS.length * LISTE_COULEURS.length;

	/**
	 * creer une instance de classe Jeu, instancie l'attribut jeuDeCartes et creer
	 * l'ensemble des instances de Carte stockées dans jeuDeCartes; l'origine du 
	 */
	public Jeu() {
		jeuDeCartes = new ArrayList<>();
		genererJeuInitial(0);

	}
	
	public Jeu(int hauteurMin) {
		jeuDeCartes = new ArrayList<>();
		genererJeuInitial(hauteurMin);
	}

	private void genererJeuInitial(int origineHauteur) {
		for (int indice = 0; indice < LISTE_COULEURS.length; indice++) {
			Carte[] suiteDeCouleur = genererSuiteDeCouleur(LISTE_COULEURS[indice], origineHauteur);
			Collections.addAll(jeuDeCartes, suiteDeCouleur);

		}

	}

	private Carte[] genererSuiteDeCouleur(String couleur, int origineHauteur) {
		Carte[] suiteCartes = new Carte[LISTE_HAUTEURS.length -origineHauteur];
		for (int indice = 0; indice < LISTE_HAUTEURS.length - origineHauteur; indice++) {
			suiteCartes[indice] = new Carte(LISTE_HAUTEURS[indice + origineHauteur], couleur);
			nombreDeCartes++;

		}
		return suiteCartes;

	}

	/**
	 * get() de nombreMaximumDeCartes = taille du jeu complet
	 * 
	 * @return le nombreMaximumDeCartes du jeu complet
	 */
	public int getNombreMaximumDeCartes() {
		return nombreMaximumDeCartes;
	}

	/**
	 * change les positions des Carte dans jeuDeCartes en utilisant
	 * Collections.shuffle(jeuDeCartes), et instancie l'attribut talon comme LinkedList contenant les cartes de jeuDeCartes
	 */
	public void melangerLesCartes() {
		Collections.shuffle(jeuDeCartes);
		talon = new LinkedList<>(jeuDeCartes);

	}

	/**
	 * vérifie si le talon existe et appelle melangerLesCartes() si non,
	 * retire une carte du talon, ajoute cette carte au début du talon et renvoie la carte retirée
	 * @return une instance de classe Carte retirée du talon
	 */

	public Carte distribuerUneCarte() {
		if (talon == null) {
			melangerLesCartes();
		}
		Carte carteDistribuee = talon.remove();
		talon.add(carteDistribuee);
		return carteDistribuee;

	}

	/**
	 * vérifie si l'attribut talon existe et appelle melangerLesCartes() si non,
	 *  retire et retourne un tableau de Carte contenant le nombre de
	 * cartes passés en paramètres, et replace les carte retirées dans le talon
	 * 
	 * @param nombreDeCartes : le nombreDeCartes à distribuer, qui sont retirées du
	 *                       talon
	 * @return un tableau d'instances de Carte retirées de jeuDeCartes
	 * 
	 */

	public Carte[] distribuerCartes(int nombreDeCartes){
		if (talon == null) {
			melangerLesCartes();
		}
		Carte[] cartesADistribuer = new Carte[nombreDeCartes];
		for (int indice = 0; indice < nombreDeCartes; indice++) {
			cartesADistribuer[indice] = distribuerUneCarte();

		}
		return cartesADistribuer;

	}

	@Override
	public String toString() {
		return "Jeu [jeuDeCartes=" + jeuDeCartes + "]";
	}

	/**
	 * get() de jeuDeCartes
	 * 
	 * @return jeuDeCartes Liste de Carte
	 */
	public List<Carte> getJeuDeCartes() {
		return jeuDeCartes;
	}

	/**
	 * set() de jeuDeCartes
	 * 
	 * @param jeuDeCartes Liste de Carte
	 */

	public void setJeuDeCartes(List<Carte> jeuDeCartes) {
		this.jeuDeCartes = (ArrayList<Carte>) jeuDeCartes;
	}

	/**
	 * get() de nombreDeCartes
	 * 
	 * @return int nombreDeCartes
	 */
	public int getNombreDeCartes() {
		return nombreDeCartes;
	}

	/**
	 * 
	 * @return int le nombre de cartes dans le jeu
	 */
	public int size() {
		return nombreDeCartes;
	}

}
