package fr.jv.pokerhands;

import java.util.ArrayList;
import java.util.Arrays;

public class Principale {
	
	static ArrayList<Joueur> listeJoueurs = new ArrayList<>(Arrays.asList(new Joueur("James Bond"), new Joueur("Le Chiffre"), new Joueur("Scaramanga"), new Joueur("Le Spectre")));

	public static void main(String[] args) throws Exception {
			Partie maPartie = new Partie(listeJoueurs);
			maPartie.executerPartie();
			System.out.println("Le gagnant de la partie est : " + maPartie.nomDuGagnantDeLaPartie());
			System.out.printf(maPartie.toString());
			System.out.println(maPartie.afficherClassement());
			
			maPartie.executerParties(10);
			System.out.println("Le gagnant du tournoi est : " + maPartie.nomDuGagnantDuTournoi());
			System.out.println(maPartie.afficherClassement());
			
		
	}
}