package fr.jv.pokerhands;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * Classe modélisant les cinq cartes distribuées au joueur d'une partie de Poker
 * Reçoit et redonne des instances de classe Carte renvoie une chaîne de
 * caractères "cotation" encodant les informations sur les combinaisons de la
 * main et sur les cartes individuelles comparable à d'autres mains par
 * comparaison des cotations
 */

//TO DO : extraire une classe calcul cotation 
//TO DO : déplacer la constante COTATIONS dans Jeu
public class MainJoueur implements Comparable<MainJoueur> {

	private static final int TAILLE_MAIN = 5;
	private static final String[] COTATIONS = { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N" };

	private static final String DEFAULT_VALUE = "A";
	private ArrayList<Carte> cartes;
	private int nombreDeCartes = 0;
	private String cotation;

	private Collator collator = Collator.getInstance(Locale.FRENCH);

	/**
	 * initialise l'attribut cartes avec une ArrayList vide
	 */
	public MainJoueur() {
		cartes = new ArrayList<>(TAILLE_MAIN);

	}

	/**
	 * initialise l'attribut cartes avec un tableau d'instances de classe Carte
	 * 
	 * @param cartesARecevoir tableau de Carte[]
	 */

	public MainJoueur(Carte[] cartesARecevoir) {
		cartes = new ArrayList<>(TAILLE_MAIN);
		Collections.addAll(cartes, cartesARecevoir);
		nombreDeCartes = cartesARecevoir.length;

	}

	/**
	 * initialise l'attribut cartes avec une List d'instances de classe Carte
	 * 
	 * @param cartesARecevoir instance de classe implémentant List d'instances de
	 *                        classe Carte[],
	 */

	public MainJoueur(List<Carte> cartesARecevoir) {
		cartes = new ArrayList<>(TAILLE_MAIN);
		for (int indice = 0; indice < cartesARecevoir.size(); indice++) {
			cartes.add(null);
		}

		Collections.copy(cartes, cartesARecevoir);
		nombreDeCartes = cartesARecevoir.size();

	}




	/**
	 * Trie les cartes en utilisant le comparateur de la classe Carte
	 * 
	 */
	public void classerCartes() {
		cartes.sort(null);

	}

	private int compare(String cotation1, String cotation2) {
		int longueurCotation = cotation1.length();
		int positionLettre = 0;
		int resultatComparaison = 0;
		while (resultatComparaison == 0 && positionLettre < longueurCotation) {
			resultatComparaison = collator.compare(cotation1.substring(positionLettre, positionLettre + 1),
					cotation2.substring(positionLettre, positionLettre + 1));
			positionLettre++;
		}
		return resultatComparaison;

	}

	@Override
	public int compareTo(MainJoueur mainJoueurAdverse) {
		this.genererCotation();
		mainJoueurAdverse.genererCotation();
		int resultatComparaison = compare(this.cotation, mainJoueurAdverse.cotation);
		if (resultatComparaison > 0)
			return 1;
		if (resultatComparaison < 0)
			return -1;
		return resultatComparaison;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MainJoueur other = (MainJoueur) obj;
		return Objects.equals(cartes, other.cartes);
	}






	/**
	 * Génère une chaine de caractères indiquant la présence ou l'absence de
	 * différentes combinaisons, la plus haute carte de chaque combinaison, et la
	 * valeur de chaque carte de la main
	 */

	public void genererCotation() {
		ConstructeurCotation constructeur = new ConstructeurCotation(this);
		cotation = constructeur.genererCotation();

	}


	/**
	 * get() de cartes;
	 * 
	 * @return l'attribut Liste d'instances de Carte de MainJoueur
	 */

	public List<Carte> getCartes() {
		return cartes;
	}

	/**
	 * get() de cotation, genere la cotation si celle-ci est nulle
	 * 
	 * @return la chaine de caractères cotation
	 */

	public String getCotation() {
		if (cotation == null) {
			genererCotation();

		}
		return cotation;
	}

	/**
	 * get() de nombreDeCartes
	 * 
	 * @return int nombreDeCartes
	 */

	public int getNombreDeCartes() {
		return nombreDeCartes;
	}


	/**
	 * ajoute une Carte dans la liste cartes et incrémente le nombreDeCartes de 1
	 * 
	 * @param carteRecue instance de Carte
	 * @throws Exception : si il y a déjà 5 cartes dans cartes ou si la carteRecue
	 *                   existe dans cartes;
	 */
	public void recevoirCarte(Carte carteRecue) throws Exception {
		int indiceOfNull = nombreDeCartes;
		if (indiceOfNull == TAILLE_MAIN) {
			throw new ArrayIndexOutOfBoundsException("La main du joueur est pleine");

		}
		if (cartes.indexOf(carteRecue) >= 0) {
			throw new Exception("Carte en double détectée!");
		}
		cartes.add(0, carteRecue);
		nombreDeCartes++;

	}

	/**
	 * ajoute les Carte de cartesRecues dans la liste cartes genere la cotation si
	 * le nombreDeCartes est égal à 5
	 * 
	 * @param cartesRecues tableau d'instances de Carte
	 * @throws Exception : si il y a déjà 5 cartes dans cartes ou si la carteRecue
	 *                   existe dans cartes;
	 */
	public void recevoirNCartes(Carte[] cartesRecues) throws Exception {
		for (Carte carteRecue : cartesRecues) {
			recevoirCarte(carteRecue);

		}

	}

	/**
	 * ajoute les Carte de cartesRecues dans la liste cartes genere la cotation si
	 * le nombreDeCartes est égal à 5
	 * 
	 * @param cartesRecues Liste d'instances de Carte
	 * @throws Exception : si il y a déjà 5 cartes dans cartes ou si la carteRecue
	 *                   existe dans cartes
	 */

	public void recevoirNCartes(List<Carte> cartesRecues) throws Exception {
		for (Carte carteRecue : cartesRecues) {
			recevoirCarte(carteRecue);

		}

	}



	/**
	 * retire la carte à l'indice cartes.size() - 1, la renvoie, et décrémente le
	 * nombreDeCartes de 1;
	 * 
	 * @return la Carte retirée de cartes;
	 * @throws Exception si nombreCartes == 0;
	 */

	public Carte rendreCarte() throws Exception {
		if (nombreDeCartes == 0) {
			throw new Exception("La main du joueur est vide");

		}
		nombreDeCartes--;
		return cartes.remove(nombreDeCartes);

	}

	/**
	 * exécute rendreCarte() n fois où n = nombreDeCartesARendre
	 * 
	 * @param nombreDeCartesARendre : entier déterminant le nombre d'exécutions de
	 *                              rendreCarte()
	 * @return un tableau d'instances de Carte contenant les cartes rendues
	 * @throws Exception si nombreCartes == 0;
	 */

	public Carte[] rendreCartes(int nombreDeCartesARendre) throws Exception {
		Carte[] cartesARendre = new Carte[nombreDeCartesARendre];
		for (int indice = 0; indice < nombreDeCartesARendre; indice++) {
			cartesARendre[indice] = rendreCarte();

		}
		return cartesARendre;

	}

	@Override
	public String toString() {
		return "MainJoueur [cartes=" + cartes + "]";

	}

	public void vider() {
		cartes.clear();
		nombreDeCartes = 0;

	}

}
