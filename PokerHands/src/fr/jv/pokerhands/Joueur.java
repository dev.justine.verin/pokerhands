package fr.jv.pokerhands;

import java.util.List;
/**
 * Classe modélisant un joueur de poker, comparable en fonction de l'attribut mainDuJoueur
 * Comprend des méthodes pour recevoir et rendre des cartes
 */
public class Joueur implements Comparable<Joueur>{

	private String nom;
	private MainJoueur mainDuJoueur;
	private int nombreDePartiesGagnees;
	
	/**
	 * Constructeur de l'instance de Joueur;
	 * instancie l'attribut mainDuJoueur par une nouvelle instance de MainJoueur() avec constructeur vide
	 * @param nomDuJoueur String donnant le nom du joueur
	 */
	public Joueur(String nomDuJoueur) {
		this.nom = nomDuJoueur;
		mainDuJoueur = new MainJoueur();
		
		
	}
	/**
	 * Constructeur
	 * instancie l'attribut mainDuJoueur par une nouvelle instance de MainJoueur() avec constructeur prenant comme paramètre un tableau de Carte
	 * @param nomDuJoueur String donnant le nom du joueur
	 * @param cartesRecues un tableau d'instances de Carte
	 */
	public Joueur(String nomDuJoueur, Carte[] cartesRecues) {
		this.nom = nomDuJoueur;
		mainDuJoueur = new MainJoueur(cartesRecues);
		
	}
	

	/**
	 * ajoute une instance de Carte à l'attribut mainDuJoueur
	 * @param carteRecue une instance de Carte
	 * @throws Exception : exceptions de mainDuJoueur.recevoirCarte();
	 */
	public void recevoirCarteJoueur(Carte carteRecue) throws Exception{
		mainDuJoueur.recevoirCarte(carteRecue);
	}
	/**
	 * ajoute les éléments du tableau Carte[] cartesRecues à mainDuJoueur
	 * @param cartesRecues un tableau d'instances de Carte
	 * @throws Exception : exceptions de mainDuJoueur.recevoirNCartes()
	 */
	public void recevoirCartesJoueur(Carte[] cartesRecues) throws Exception {
		mainDuJoueur.recevoirNCartes(cartesRecues);
		
		
	}
	/**
	 * ajoute les éléments de la liste de Carte cartesRecues à mainDuJoueur
	 * @param cartesRecues un tableau d'instances de Carte
	 * @throws Exception : exceptions de mainDuJoueur.recevoirNCartes()
	 */
	
	public void recevoirCartesJoueur(List<Carte> cartesRecues) throws Exception{
		mainDuJoueur.recevoirNCartes(cartesRecues);
	}
	
	
	public void gagnerUnePartie() {
		nombreDePartiesGagnees++;
		
		
	}
	/**
	 * get() de nom
	 * @return l'attribut String nom de Joueur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * setter de nom
	 * @param nom String remplaçant ou assignant une valeur à l'attribut nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * getter de mainDuJoueur
	 * @return l'attribut MainJoueur mainDuJoueur de joueur
	 */
	public MainJoueur getMainDuJoueur() {
		return mainDuJoueur;
	}
	/**
	 * setter de mainDuJoueur
	 * @param mainDuJoueur une instance de MainJoueur assignant ou remplaçant une valeur à l'attribut mainDuJoueur
	 */
	public void setMainDuJoueur(MainJoueur mainDuJoueur) {
		this.mainDuJoueur = mainDuJoueur;
	}

	@Override
	public String toString() {
		return "Joueur [nom=" + nom + ", " +" cotation=  " + mainDuJoueur.getCotation() + ", " + mainDuJoueur + "]%n";
	}

	public int compareTo(Joueur joueurAdverse) {
		return mainDuJoueur.compareTo(joueurAdverse.getMainDuJoueur());
	}
	public int getNombreDePartiesGagnees() {
		return nombreDePartiesGagnees;
	}
	
	
	
	
}
