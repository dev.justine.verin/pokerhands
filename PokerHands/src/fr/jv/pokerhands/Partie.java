package fr.jv.pokerhands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Classe modélisant une partie de poker réalisée avec une liste de joueurs de
 * classe Joueur et un jeu de carte de classe Jeu Ajoute des joueurs, réalise
 * une partie et affiche le nom du gagnant de la partie
 */


//TO DO : garder traces des combinaisons 
//TO DO : implémenter le couple observeur (affichage résultats) / observable (partie)

public class Partie {

	private ArrayList<Joueur> joueurs;
	private Jeu jeu;
	private int nombreJoueurs;
	private static final int NOMBRE_MIN_JOUEURS = 2;
	private static final int NOMBRE_MAX_JOUEURS = 8;
	private Comparator<Joueur> comparerNombrePartiesGagnees = (Joueur joueur1, Joueur joueur2) -> {
		Integer nombrePartiesGagneesJoueur1 = joueur1.getNombreDePartiesGagnees();
		Integer nombrePartiesGagneesJoueur2 = joueur2.getNombreDePartiesGagnees();
		return nombrePartiesGagneesJoueur1.compareTo(nombrePartiesGagneesJoueur2);

	};

	/**
	 * Crée une nouvelle instance de partie en instanciant l'attribut jeu et en
	 * créant une liste vide de Joueurs;
	 */
	public Partie() {
		this.jeu = new Jeu();
		joueurs = new ArrayList<>();
	}

	/**
	 * Crée une nouvelle instance de partie en instanciant l'attribut jeu et en
	 * créant une liste de joueurs à partir de la liste passée en paramètre
	 * 
	 * @param listeJoueurs Liste de Joueur la liste des joueurs de la partie;
	 */
	public Partie(List<Joueur> listeJoueurs) {
		this.jeu = new Jeu();
		if (listeJoueurs.size() > 8) {
			System.out.println("Limite du nombre joueurs atteinte; seuls les 8 premiers joueurs participeront");
			ajouterJoueurs((ArrayList<Joueur>) listeJoueurs);
			return;
		}
		this.joueurs = (ArrayList<Joueur>) listeJoueurs;
		nombreJoueurs = this.joueurs.size();

	}

	/**
	 * Crée une nouvelle instance de partie en instanciant l'attribut jeu et en
	 * créant une liste de joueurs à partir du tableau passé en paramètre
	 * 
	 * @param tableauJoueurs tableau d'instances de Joueur
	 * 
	 */
	public Partie(Joueur[] tableauJoueurs) {
		this.jeu = new Jeu();
		if (tableauJoueurs.length > 8) {
			System.out.println("Limite du nombre joueurs atteinte; seuls les 8 premiers joueurs participeront");
			ajouterJoueurs(tableauJoueurs);

		}
		Collections.addAll(joueurs, tableauJoueurs);

	}

	/**
	 * Ajoute des joueurs à l'attribut joueurs à partir de la liste listeJoueurs
	 * 
	 * @param listeJoueurs une liste d'instances de Joueurs
	 */
	public void ajouterJoueurs(ArrayList<Joueur> listeJoueurs) {
		int nombreActuelDeJoueurs = nombreJoueurs;
		for (int indice = 0; indice < NOMBRE_MAX_JOUEURS - nombreActuelDeJoueurs; indice++) {
			joueurs.add(listeJoueurs.get(indice));
			nombreJoueurs++;
		}

	}

	/**
	 * Ajoute des joueurs à l'attribut joueurs à partir du tableau Joueur[]
	 * tableauJoueurs
	 * 
	 * @param tableauJoueurs tableau d'instances de Joueur
	 */

	public void ajouterJoueurs(Joueur[] tableauJoueurs) {
		int nombreActuelDeJoueurs = nombreJoueurs;
		for (int indice = 0; indice < NOMBRE_MAX_JOUEURS - nombreActuelDeJoueurs; indice++) {
			Arrays.asList(tableauJoueurs[indice]);
			nombreJoueurs++;

		}

	}

	public void executerPartie() throws Exception {
		if (nombreJoueurs < NOMBRE_MIN_JOUEURS) {
			throw new Exception("Nombre insuffisant de joueurs pour commencer la partie");
		}
		jeu.melangerLesCartes();
		for (Joueur joueur : joueurs) {
			joueur.getMainDuJoueur().vider();
			joueur.recevoirCartesJoueur(jeu.distribuerCartes(5));
		}
		joueurs.sort(null);
		Joueur joueurGagnant = joueurs.get(nombreJoueurs - 1);
		joueurGagnant.gagnerUnePartie();

	}

	public void executerParties(int nombreDePartiesAJouer) throws Exception {
		for(int numeroPartie = 1; numeroPartie <= nombreDePartiesAJouer; numeroPartie++) {
			executerPartie();
		}
		joueurs.sort(comparerNombrePartiesGagnees);
		
	}
	

	/**
	 * Exécute une partie et renvoie le nom du joueur gagnant (le joueur le mieux
	 * classé)
	 * 
	 * @return String nomDuGagnant : l'attribut nom du joueur gagnant
	 * @throws Exception si il y a moins de deux joueurs dans la partie
	 */
	public String nomDuGagnantDeLaPartie() {
		return joueurs.get(nombreJoueurs - 1).getNom();

	}

	public String nomDuGagnantDuTournoi() {
		return joueurs.get(nombreJoueurs - 1).getNom();
	}

	public String afficherClassement() {
		
		StringBuilder classement = new StringBuilder("Classement: ");
		classement.append(String.format("%n"));
		for(int rang = 1; rang <= nombreJoueurs; rang++) {
			Joueur joueur = joueurs.get(nombreJoueurs - rang);
			classement.append(rang + ":");
			classement.append(joueur.getNom());
			classement.append("(" + joueur.getNombreDePartiesGagnees() + " partie(s) gagnée(s))");
			classement.append(String.format("%n"));
			
			
		}
		return classement.toString();
	}

	@Override
	public String toString() {
		return String.format("Liste des joueurs ; %n %s %n", joueurs);
	}

}
